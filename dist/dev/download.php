<?php

error_reporting( E_ALL );
ini_set('display_errors', 1);

$file = $_GET['file'];

header("Content-type:application/pdf");
header("Content-Disposition:attachment;filename='".$file.".pdf'");
readfile("documents/".$file.".pdf");
exit();
